FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/citizen-financial-claim-process/api
COPY ./cmd /go/src/citizen-financial-claim-process/cmd
COPY ./internal /go/src/citizen-financial-claim-process/internal
COPY ./pkg /go/src/citizen-financial-claim-process/pkg
COPY ./go.mod /go/src/citizen-financial-claim-process/
COPY ./go.sum /go/src/citizen-financial-claim-process/
WORKDIR /go/src/citizen-financial-claim-process
RUN go mod download \
 && go build -o dist/bin/citizen-financial-claim-process ./cmd/citizen-financial-claim-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/citizen-financial-claim-process/dist/bin/citizen-financial-claim-process /usr/local/bin/citizen-financial-claim-process
COPY --from=build /go/src/citizen-financial-claim-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/citizen-financial-claim-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/citizen-financial-claim-process"]
