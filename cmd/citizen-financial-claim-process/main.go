// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	financialClaimRequestRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/repositories"
	sessionServiceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/events"
)

type options struct {
	ListenAddress                       string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the citizen-financial-claim-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	FinancialClaimRequestServiceAddress string `long:"financial-claim-request-service-address" env:"FINANCIAL_CLAIM_REQUEST_SERVICE_ADDRESS" default:"http://localhost:80" description:"Financial claim request service address."`
	FinancialClaimRequestServiceAPIKey  string `long:"financial-claim-request-service-api-key" env:"FINANCIAL_CLAIM_REQUEST_SERVICE_API_KEY" default:"" description:"API key to use when calling the Financial claim request service."`
	SessionServiceAddress               string `long:"session-service-address" env:"SESSION_SERVICE_ADDRESS" default:"http://localhost:80" description:"Session service address."`
	SessionServiceAPIKey                string `long:"session-service-api-key" env:"SESSION_SERVICE_API_KEY" default:"" description:"API key to use when calling the session service."`
	SourceSystemAddress                 string `long:"source-system-address" env:"SOURCE_SYSTEM_ADDRESS" default:"" description:"Source system address."`
	ConfigurationExpirationMinutes      string `long:"configuration-expiration-minutes" env:"CONFIGURATION_EXPIRATION_MINUTES" default:"60" description:"Max length of a configuration in minutes."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	financialClaimRequestRepository := financialClaimRequestRepositories.NewFinancialClaimRequestClient(cliOptions.FinancialClaimRequestServiceAddress, cliOptions.FinancialClaimRequestServiceAPIKey)

	sessionRepository := sessionServiceRepositories.NewSessionClient(cliOptions.SessionServiceAddress, cliOptions.SessionServiceAPIKey)

	configurationExpirationMinutes, err := strconv.Atoi(cliOptions.ConfigurationExpirationMinutes)
	if err != nil {
		event := events.CFCP_45
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	financialClaimsInformationRepository := repositories.NewFinancialClaimsInformationClient(cliOptions.SourceSystemAddress, logger)

	citizenFinancialClaimUseCase := usecases.NewCitizenFinancialClaimUseCase(
		logger,
		financialClaimRequestRepository,
		sessionRepository,
		financialClaimsInformationRepository,
		configurationExpirationMinutes,
	)

	router := http_infra.NewRouter(citizenFinancialClaimUseCase, logger)

	event := events.CFCP_42
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.CFCP_43
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	zapLogger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return zapLogger, nil
}
