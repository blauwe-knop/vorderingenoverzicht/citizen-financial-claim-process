// Code generated by MockGen. DO NOT EDIT.
// Source: ./../../internal/repositories/financial_claims_information_repository.go
//
// Generated by this command:
//
//	mockgen -destination=./mock_financial_claims_information_repository.go -package=mock -source=./../../internal/repositories/financial_claims_information_repository.go
//

// Package mock is a generated GoMock package.
package mock

import (
	reflect "reflect"

	healthcheck "gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	model "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	gomock "go.uber.org/mock/gomock"
)

// MockFinancialClaimsInformationRepository is a mock of FinancialClaimsInformationRepository interface.
type MockFinancialClaimsInformationRepository struct {
	ctrl     *gomock.Controller
	recorder *MockFinancialClaimsInformationRepositoryMockRecorder
	isgomock struct{}
}

// MockFinancialClaimsInformationRepositoryMockRecorder is the mock recorder for MockFinancialClaimsInformationRepository.
type MockFinancialClaimsInformationRepositoryMockRecorder struct {
	mock *MockFinancialClaimsInformationRepository
}

// NewMockFinancialClaimsInformationRepository creates a new mock instance.
func NewMockFinancialClaimsInformationRepository(ctrl *gomock.Controller) *MockFinancialClaimsInformationRepository {
	mock := &MockFinancialClaimsInformationRepository{ctrl: ctrl}
	mock.recorder = &MockFinancialClaimsInformationRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockFinancialClaimsInformationRepository) EXPECT() *MockFinancialClaimsInformationRepositoryMockRecorder {
	return m.recorder
}

// Get mocks base method.
func (m *MockFinancialClaimsInformationRepository) Get(id model.UserIdentity) (*[]byte, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", id)
	ret0, _ := ret[0].(*[]byte)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get indicates an expected call of Get.
func (mr *MockFinancialClaimsInformationRepositoryMockRecorder) Get(id any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockFinancialClaimsInformationRepository)(nil).Get), id)
}

// GetHealthCheck mocks base method.
func (m *MockFinancialClaimsInformationRepository) GetHealthCheck() healthcheck.Result {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetHealthCheck")
	ret0, _ := ret[0].(healthcheck.Result)
	return ret0
}

// GetHealthCheck indicates an expected call of GetHealthCheck.
func (mr *MockFinancialClaimsInformationRepositoryMockRecorder) GetHealthCheck() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetHealthCheck", reflect.TypeOf((*MockFinancialClaimsInformationRepository)(nil).GetHealthCheck))
}
