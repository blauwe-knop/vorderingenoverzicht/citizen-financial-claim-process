//go:build tools

//go:generate go run go.uber.org/mock/mockgen -destination=./mock_financial_claims_information_repository.go -package=mock -source=./../../internal/repositories/financial_claims_information_repository.go
//go:generate go run go.uber.org/mock/mockgen -destination=./mock_financial_claim_request_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/repositories FinancialClaimRequestRepository
//go:generate go run go.uber.org/mock/mockgen -destination=./mock_session_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories SessionRepository
//go:generate go run go.uber.org/mock/mockgen -destination=./mock_bk_config_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories BkConfigRepository
//go:generate go run go.uber.org/mock/mockgen -destination=./mock_scheme_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories SchemeRepository

package mock

import (
	_ "go.uber.org/mock/mockgen"
)
