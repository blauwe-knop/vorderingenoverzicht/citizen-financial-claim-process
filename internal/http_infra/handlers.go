// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/events"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/usecases"
)

func handlerConfigureClaimsRequest(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	citizenFinancialClaimUseCase, _ := context.Value(citizenFinancialClaimUseCaseKey).(*usecases.CitizenFinancialClaimUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	sessionToken := request.Header.Get("Authorization")

	event := events.CFCP_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))

	var encodedAndEncryptedDocumentJWS string
	err := json.NewDecoder(request.Body).Decode(&encodedAndEncryptedDocumentJWS)
	if err != nil {
		event := events.CFCP_39
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	encryptedConfigurationToken, err := citizenFinancialClaimUseCase.ConfigureClaimsRequest(sessionToken, encodedAndEncryptedDocumentJWS)
	if errors.Is(err, usecases.ErrInvalidRequest) {
		event := events.CFCP_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "bad request", http.StatusBadRequest)
		return
	}
	if errors.Is(err, usecases.ErrSessionExpired) {
		event := events.CFCP_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "session expired", http.StatusUnauthorized)
		return
	}
	if errors.Is(err, usecases.ErrCertificateExpired) {
		event := events.CFCP_15
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "certificate expired", http.StatusUnauthorized)
		return
	}
	if err != nil {
		event := events.CFCP_41
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(encryptedConfigurationToken)
	if err != nil {
		event := events.CFCP_37
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.CFCP_44
	logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))
}

func handlerRequestFinancialClaimsInformation(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	citizenFinancialClaimUseCase, _ := context.Value(citizenFinancialClaimUseCaseKey).(*usecases.CitizenFinancialClaimUseCase)
	logger := context.Value(loggerKey).(*zap.Logger)

	sessionToken := request.Header.Get("Authorization")
	configurationToken := request.URL.Query().Get("configurationToken")

	event := events.CFCP_25
	logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))

	financialClaimsInformation, err := citizenFinancialClaimUseCase.RequestFinancialClaimsInformation(sessionToken, configurationToken)
	if errors.Is(err, usecases.ErrSessionExpired) {
		event := events.CFCP_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "session expired", http.StatusUnauthorized)
		return
	}
	if err != nil {
		event := events.CFCP_38
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(financialClaimsInformation)
	if err != nil {
		event := events.CFCP_37
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.CFCP_26
	logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))
}
