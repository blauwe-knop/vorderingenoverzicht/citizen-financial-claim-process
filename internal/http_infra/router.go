// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/http_infra/metrics"

	"github.com/go-chi/cors"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/versions"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
)

type key int

const (
	citizenFinancialClaimUseCaseKey key = iota
	loggerKey                       key = iota
)

func NewRouter(citizenFinancialClaimUseCase *usecases.CitizenFinancialClaimUseCase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Authorization"},
	}))

	metricsMiddleware := metrics.NewMiddleware("citizen-financial-claim-process")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/"+versions.CitizenFinancialClaimsProcessApiVersion, func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "3.0.0"))

		r.Route("/configure_claims_request", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), citizenFinancialClaimUseCaseKey, citizenFinancialClaimUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerConfigureClaimsRequest(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/request_financial_claims_information", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), citizenFinancialClaimUseCaseKey, citizenFinancialClaimUseCase)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerRequestFinancialClaimsInformation(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("citizen-financial-claim-process", citizenFinancialClaimUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	r.Route("/.well-known", func(r chi.Router) {
		r.Get("/version", func(responseWriter http.ResponseWriter, request *http.Request) {
			responseWriter.WriteHeader(http.StatusOK)
			responseWriter.Header().Set("Content-Type", "application/json")

			version := model.Version{
				CitizenFinancialClaimsProcessComponentVersion: versions.CitizenFinancialClaimsProcessComponentVersion,
				CitizenFinancialClaimsProcessApiVersion:       versions.CitizenFinancialClaimsProcessApiVersion,
				RequestDocumentVersion:                        versions.RequestDocumentVersion,
				FinancialClaimsInformationDocumentVersion:     versions.FinancialClaimsInformationDocumentVersion,
			}
			err := json.NewEncoder(responseWriter).Encode(version)
			if err != nil {
				event := events.CFCP_37
				logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
				http.Error(responseWriter, "internal error", http.StatusInternalServerError)
				return
			}
		})
	})

	return r
}
