package usecases_test

import (
	"crypto/elliptic"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jwe"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jws"
	"go.uber.org/mock/gomock"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/test/mock"

	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	sessionServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/repositories"
	sessionServiceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
)

func TestCitizenFinancialClaimUseCase(t *testing.T) {
	t.Run("Configure claims request", func(t *testing.T) {
		observedZapCore, _ := observer.New(zap.InfoLevel)
		observedLogger := zap.New(observedZapCore)

		appKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
		assert.NoError(t, err)

		sessionAesKey, err := aes.GenerateKey(aes.AES128)
		assert.NoError(t, err)

		type fields struct {
			FinancialClaimRequestRepository serviceRepositories.FinancialClaimRequestRepository
			SessionRepository               sessionServiceRepositories.SessionRepository
			ConfigurationExpirationMinutes  int
		}
		type args struct {
			sessionToken string
			documentJwe  string
		}

		tests := []struct {
			name          string
			fields        fields
			args          args
			expectedError error
		}{
			{
				name: "configure a claims request with a valid session token and a correct document results in a configuration token",
				fields: fields{
					FinancialClaimRequestRepository: createFinancialClaimRequestRepositoryMock(t, appKeyPair),
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "token"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "token",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: nil,
			},
			{
				name: "configure a claims request with an empty session token results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "session-empty"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "session-empty",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("get session failed error, for session (session-empty): session token is empty"),
			},
			{
				name: "configure a claims request with an expired session token results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "session-expired"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "session-expired",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("error session expired"),
			},
			{
				name: "configure a claims request with a session token with a wrong scope results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "session-wrong-scope"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "session-wrong-scope",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("certificate with scope wrong-scope is unsupported"),
			},
			{
				name: "configure a claims request with a session token with a wrong aes key results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "session-wrong-aes-key"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "session-wrong-aes-key",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("failed to parse session aes key to bytes: illegal base64 data at input byte 12"),
			},
			{
				name: "configure a claims request with a session token with a wrong app public key results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "session-wrong-app-public-key"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "session-wrong-app-public-key",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("failed to decode public key from pem: failed to parse public key"),
			},
			{
				name: "configure a claims request with a signature not matching the document results in an error",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "token"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "token",
					documentJwe:  createDocumentJWENotMatchingSignature(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("invalid document signature"),
			},
			{
				name: "configure a claims request with an error while creating configuration token results in an error",
				fields: fields{
					FinancialClaimRequestRepository: createFinancialClaimRequestRepositoryWithErrorMock(t),
					SessionRepository:               createSessionServiceRepositoryMock(t, appKeyPair, sessionAesKey, "token"),
					ConfigurationExpirationMinutes:  5,
				},
				args: args{
					sessionToken: "token",
					documentJwe:  createDocumentJWE(appKeyPair, sessionAesKey),
				},
				expectedError: fmt.Errorf("failed to create configuration: problem creating configuration"),
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				citizenFinancialClaimUseCase := usecases.NewCitizenFinancialClaimUseCase(
					observedLogger,
					tt.fields.FinancialClaimRequestRepository,
					tt.fields.SessionRepository,
					nil,
					tt.fields.ConfigurationExpirationMinutes,
				)

				configurationTokenDocumentJWE, err := citizenFinancialClaimUseCase.ConfigureClaimsRequest(
					tt.args.sessionToken,
					tt.args.documentJwe)

				assert.Equal(t, tt.expectedError, err)
				if tt.expectedError == nil {
					configurationTokenDocumentJWS, err := jwe.ToPlaintext(sessionAesKey, configurationTokenDocumentJWE)
					assert.NoError(t, err, "configuration token JWE Document not correct")

					configurationTokenPayloadAsJson, err := jws.ToPlaintext(string(configurationTokenDocumentJWS))
					assert.NoError(t, err, "configuration token JWS Document not correct")

					configurationTokenPayload, err := model.ConfigurationTokenPayloadFromJson(configurationTokenPayloadAsJson)
					assert.NoError(t, err, "configuration token payload not properly formatted JSON")
					assert.Equal(t, "configuration_token", configurationTokenPayload.Sub, "configuration token JWS payload subject not correct")
					assert.Equal(t, "https://citizen-financial-claims-process.example.com/", configurationTokenPayload.Iss, "configuration token JWS payload issuer not correct")
					assert.NotEmpty(t, configurationTokenPayload.ConfigurationToken, "configuration token empty")
					_, err = uuid.Parse(configurationTokenPayload.ConfigurationToken)
					assert.NoError(t, err, "configuration token not a valid uuid")
				}
			})
		}
	})

	t.Run("Request financial claims information", func(t *testing.T) {
		observedZapCore, _ := observer.New(zap.InfoLevel)

		observedLogger := zap.New(observedZapCore)

		appKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
		assert.NoError(t, err)

		appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
		assert.NoError(t, err)

		sessionAesKey, err := aes.GenerateKey(aes.AES128)
		assert.NoError(t, err)

		configurationToken := "configurationToken"

		var financialClaimsInformationDocument sourceSystemModel.FinancialClaimsInformationDocument
		financialClaimsInformationDocument.Type = "1"
		financialClaimsInformationDocument.Version = "1"
		financialClaimsInformationDocument.Body.DocumentDatumtijd = time.Now()
		financialClaimsInformationDocument.Body.Bsn = "123456789"
		financialClaimsInformationDocument.Body.AangeleverdDoor = "TEST"
		var financieleZaak sourceSystemModel.FinancialClaimsInformationDocumentFinancieleZaak
		financieleZaak.Bsn = "123456789"
		financieleZaak.Saldo = 100
		financieleZaak.SaldoDatumtijd = time.Now()
		financieleZaak.TotaalFinancieelVereffend = 0
		financieleZaak.TotaalFinancieelVerplicht = 0
		financieleZaak.Achterstanden = nil
		financieleZaak.Gebeurtenissen = nil
		financialClaimsInformationDocument.Body.FinancieleZaken = append(financialClaimsInformationDocument.Body.FinancieleZaken, financieleZaak)
		financialClaimsInformationDocumentAsJson, _ := json.Marshal(financialClaimsInformationDocument)

		type fields struct {
			FinancialClaimRequestRepository      serviceRepositories.FinancialClaimRequestRepository
			SessionRepository                    sessionServiceRepositories.SessionRepository
			FinancialClaimsInformationRepository repositories.FinancialClaimsInformationRepository
		}
		type args struct {
			sessionToken       string
			configurationToken string
		}

		tests := []struct {
			name                               string
			fields                             fields
			args                               args
			expectedError                      error
			expectedFinancialClaimsInformation string
		}{
			{
				name: "return financial claims information when using valid session and configuration token",
				fields: fields{
					FinancialClaimRequestRepository: func() serviceRepositories.FinancialClaimRequestRepository {
						controller := gomock.NewController(t)
						financialClaimRequestRepository := mock.NewMockFinancialClaimRequestRepository(controller)
						dateTime := time.Now()
						financialClaimRequestRepository.
							EXPECT().
							Get(configurationToken).
							Return(
								&serviceModel.Configuration{
									Token:        configurationToken,
									Bsn:          "123456789",
									CreatedAt:    serviceModel.JSONTime(dateTime),
									ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
									AppPublicKey: string(appPublicKeyPem),
								},
								nil,
							)

						return financialClaimRequestRepository
					}(),
					SessionRepository: func() sessionServiceRepositories.SessionRepository {
						controller := gomock.NewController(t)
						sessionRepository := mock.NewMockSessionRepository(controller)
						sessionToken := "token"
						dateTime := time.Now()
						sessionRepository.
							EXPECT().
							GetSession(sessionToken).
							Return(
								&sessionServiceModel.Session{
									Token:        sessionToken,
									AesKey:       base64.ParseFromBytes(sessionAesKey),
									AppPublicKey: string(appPublicKeyPem),
									Bsn:          "123456789",
									Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
									CreatedAt:    sessionServiceModel.JSONTime(dateTime),
									ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								},
								nil,
							)

						return sessionRepository
					}(),
					FinancialClaimsInformationRepository: func() repositories.FinancialClaimsInformationRepository {
						controller := gomock.NewController(t)
						financialClaimsInformationRepository := mock.NewMockFinancialClaimsInformationRepository(controller)
						financialClaimsInformationRepository.
							EXPECT().
							Get(sourceSystemModel.UserIdentity{Bsn: "123456789"}).
							Return(
								&financialClaimsInformationDocumentAsJson,
								nil,
							)
						return financialClaimsInformationRepository
					}(),
				},
				args: args{
					sessionToken:       "token",
					configurationToken: configurationToken,
				},
				expectedError:                      nil,
				expectedFinancialClaimsInformation: string(financialClaimsInformationDocumentAsJson),
			},
			{
				name: "return session expired error when session is expired",
				fields: fields{
					FinancialClaimRequestRepository: nil,
					SessionRepository: func() sessionServiceRepositories.SessionRepository {
						controller := gomock.NewController(t)
						sessionRepository := mock.NewMockSessionRepository(controller)
						sessionToken := "token"
						dateTime := time.Now().Add(time.Minute * -10)
						sessionRepository.
							EXPECT().
							GetSession(sessionToken).
							Return(
								&sessionServiceModel.Session{
									Token:        sessionToken,
									AppPublicKey: string(appPublicKeyPem),
									Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
									CreatedAt:    sessionServiceModel.JSONTime(dateTime),
									ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								},
								nil,
							)
						return sessionRepository
					}(),
					FinancialClaimsInformationRepository: nil,
				},
				args: args{
					sessionToken: "token",
				},
				expectedError:                      usecases.ErrSessionExpired,
				expectedFinancialClaimsInformation: "",
			},
			{
				name: "return configuration expired error when configuration token is expired",
				fields: fields{
					SessionRepository: func() sessionServiceRepositories.SessionRepository {
						controller := gomock.NewController(t)
						sessionRepository := mock.NewMockSessionRepository(controller)
						sessionToken := "token"
						dateTime := time.Now()
						sessionRepository.
							EXPECT().
							GetSession(sessionToken).
							Return(
								&sessionServiceModel.Session{
									Token:        sessionToken,
									AppPublicKey: string(appPublicKeyPem),
									AesKey:       base64.ParseFromBytes(sessionAesKey),
									Bsn:          "123456789",
									Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
									CreatedAt:    sessionServiceModel.JSONTime(dateTime),
									ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
								},
								nil,
							)
						return sessionRepository
					}(),
					FinancialClaimRequestRepository: func() serviceRepositories.FinancialClaimRequestRepository {
						controller := gomock.NewController(t)
						financialClaimRequestRepository := mock.NewMockFinancialClaimRequestRepository(controller)
						dateTime := time.Now().Add(time.Minute * -5)
						financialClaimRequestRepository.
							EXPECT().
							Get(configurationToken).
							Return(
								&serviceModel.Configuration{
									Token:        configurationToken,
									Bsn:          "123456789",
									CreatedAt:    serviceModel.JSONTime(dateTime),
									ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
									AppPublicKey: string(appPublicKeyPem),
								},
								nil,
							)
						return financialClaimRequestRepository
					}(),
					FinancialClaimsInformationRepository: nil,
				},
				args: args{
					sessionToken:       "token",
					configurationToken: configurationToken,
				},
				expectedError:                      usecases.ErrConfigurationExpired,
				expectedFinancialClaimsInformation: "",
			},
		}

		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				citizenFinancialClaimUseCase := usecases.NewCitizenFinancialClaimUseCase(
					observedLogger,
					tt.fields.FinancialClaimRequestRepository,
					tt.fields.SessionRepository,
					tt.fields.FinancialClaimsInformationRepository,
					0,
				)

				encodedEncryptedFinancialClaimsInformation, err := citizenFinancialClaimUseCase.RequestFinancialClaimsInformation(
					tt.args.sessionToken,
					tt.args.configurationToken)

				if err == nil {
					encryptedFinancialClaimsInformation, err := encodedEncryptedFinancialClaimsInformation.ToBytes()
					assert.NoError(t, err)
					nonceSize := 12
					nonce := encryptedFinancialClaimsInformation[:nonceSize]
					authenticationTagSize := 16
					authenticationTag := encryptedFinancialClaimsInformation[len(encryptedFinancialClaimsInformation)-authenticationTagSize:]
					encryptedFinancialClaimsInformation = encryptedFinancialClaimsInformation[nonceSize : len(encryptedFinancialClaimsInformation)-authenticationTagSize]
					financialClaimsInformation, err := aes.Decrypt(sessionAesKey, nonce, encryptedFinancialClaimsInformation, authenticationTag)
					assert.NoError(t, err)
					assert.Equal(t, tt.expectedFinancialClaimsInformation, string(financialClaimsInformation))
				}
				assert.Equal(t, tt.expectedError, err)
			})
		}
	})
}

func createSessionServiceRepositoryMock(t *testing.T, appKeyPair *ec.PrivateKey, sessionAesKey []byte, sessionToken string) sessionServiceRepositories.SessionRepository {
	controller := gomock.NewController(t)

	sessionRepository := mock.NewMockSessionRepository(controller)

	dateTime := time.Now()
	appPublicKey, err := appKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	if sessionToken == "session-empty" {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				nil,
				errors.New("session token is empty"),
			)
	} else if sessionToken == "session-expired" {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				&sessionServiceModel.Session{
					Token:        sessionToken,
					AppPublicKey: string(appPublicKey),
					AesKey:       base64.ParseFromBytes(sessionAesKey),
					Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
					Bsn:          "123456789",
					CreatedAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * -15)),
					ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * -5)),
				},
				nil,
			)
	} else if sessionToken == "session-wrong-scope" {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				&sessionServiceModel.Session{
					Token:        sessionToken,
					AppPublicKey: string(appPublicKey),
					AesKey:       base64.ParseFromBytes(sessionAesKey),
					Scope:        "wrong-scope",
					Bsn:          "123456789",
					CreatedAt:    sessionServiceModel.JSONTime(dateTime),
					ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
				},
				nil,
			)
	} else if sessionToken == "session-wrong-aes-key" {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				&sessionServiceModel.Session{
					Token:        sessionToken,
					AppPublicKey: string(appPublicKey),
					AesKey:       "wrong-aes-key",
					Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
					Bsn:          "123456789",
					CreatedAt:    sessionServiceModel.JSONTime(dateTime),
					ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
				},
				nil,
			)
	} else if sessionToken == "session-wrong-app-public-key" {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				&sessionServiceModel.Session{
					Token:        sessionToken,
					AppPublicKey: "wrong-app-public-key",
					AesKey:       base64.ParseFromBytes(sessionAesKey),
					Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
					Bsn:          "123456789",
					CreatedAt:    sessionServiceModel.JSONTime(dateTime),
					ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
				},
				nil,
			)
	} else {
		sessionRepository.
			EXPECT().
			GetSession(sessionToken).
			Return(
				&sessionServiceModel.Session{
					Token:        sessionToken,
					AppPublicKey: string(appPublicKey),
					AesKey:       base64.ParseFromBytes(sessionAesKey),
					Scope:        "nl.vorijk.oauth_scope.blauwe_knop",
					Bsn:          "123456789",
					CreatedAt:    sessionServiceModel.JSONTime(dateTime),
					ExpiresAt:    sessionServiceModel.JSONTime(dateTime.Add(time.Minute * 5)),
				},
				nil,
			)
	}
	return sessionRepository
}

func createFinancialClaimRequestRepositoryMock(t *testing.T, appKeyPair *ec.PrivateKey) serviceRepositories.FinancialClaimRequestRepository {
	controller := gomock.NewController(t)

	bsn := "123456789"

	dateTime := time.Now()
	appPublicKey, err := appKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	configuration := serviceModel.Configuration{
		Token:        "ac36ff08-c353-4698-8e51-78ff7d582542",
		AppPublicKey: string(appPublicKey),
		Bsn:          bsn,
		CreatedAt:    serviceModel.JSONTime(dateTime),
		ExpiresAt:    serviceModel.JSONTime(dateTime.Add(time.Minute * 5)),
	}

	financialClaimRequestRepository := mock.NewMockFinancialClaimRequestRepository(controller)

	financialClaimRequestRepository.
		EXPECT().
		Create(gomock.Any()).
		Return(&configuration, nil)

	return financialClaimRequestRepository
}

func createFinancialClaimRequestRepositoryWithErrorMock(t *testing.T) serviceRepositories.FinancialClaimRequestRepository {
	controller := gomock.NewController(t)

	financialClaimRequestRepository := mock.NewMockFinancialClaimRequestRepository(controller)

	financialClaimRequestRepository.
		EXPECT().
		Create(gomock.Any()).
		Return(nil, errors.New("problem creating configuration"))

	return financialClaimRequestRepository
}

func createDocumentJWE(appKeyPair *ec.PrivateKey, sessionAesKey []byte) string {
	documentPayload := model.DefaultDocumentPayload("FINANCIAL_CLAIMS_INFORMATION_REQUEST")
	documentPayloadAsEncodedJson, _ := documentPayload.AsEncodedJson()

	documentJwsAsSignedEncodedString, _ := jws.NewDefaultSignedDocument([]byte(documentPayloadAsEncodedJson), appKeyPair)
	documentJwe, _ := jwe.NewDefaultDocument(sessionAesKey, []byte(documentJwsAsSignedEncodedString))

	return documentJwe
}

func createDocumentJWENotMatchingSignature(appKeyPair *ec.PrivateKey, sessionAesKey []byte) string {
	documentPayload := model.DefaultDocumentPayload("FINANCIAL_CLAIMS_INFORMATION_REQUEST")
	documentPayloadAsEncodedJson, _ := documentPayload.AsEncodedJson()

	signedDocumentJws, _ := jws.NewDefaultSignedDocument([]byte(documentPayloadAsEncodedJson), appKeyPair)

	splittedSignedDocumentJws := strings.Split(signedDocumentJws, ".")
	alteredDocumentJws := fmt.Sprintf("%s.%s.%s", splittedSignedDocumentJws[0], base64.ParseFromBytes([]byte("SOMETHING_ELSE")), splittedSignedDocumentJws[1])

	documentJwe, _ := jwe.NewDefaultDocument(sessionAesKey, []byte(alteredDocumentJws))
	return documentJwe
}
