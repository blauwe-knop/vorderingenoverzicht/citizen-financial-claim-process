// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jwe"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/jwt/jws"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"

	"github.com/google/uuid"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/repositories"
	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	sessionServiceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/events"
)

type CitizenFinancialClaimUseCase struct {
	Logger                               *zap.Logger
	FinancialClaimRequestRepository      serviceRepositories.FinancialClaimRequestRepository
	SessionRepository                    sessionServiceRepositories.SessionRepository
	FinancialClaimsInformationRepository repositories.FinancialClaimsInformationRepository
	ConfigurationExpirationMinutes       int
}

var ErrInvalidRequest = errors.New("error invalid request")
var ErrSessionExpired = errors.New("error session expired")
var ErrCertificateExpired = errors.New("error certificate expired")
var ErrConfigurationExpired = errors.New("error configuration expired")

func NewCitizenFinancialClaimUseCase(logger *zap.Logger, financialClaimRequestRepository serviceRepositories.FinancialClaimRequestRepository, sessionRepository sessionServiceRepositories.SessionRepository, financialClaimsInformationRepository repositories.FinancialClaimsInformationRepository, configurationExpirationMinutes int) *CitizenFinancialClaimUseCase {
	return &CitizenFinancialClaimUseCase{
		Logger:                               logger,
		FinancialClaimRequestRepository:      financialClaimRequestRepository,
		SessionRepository:                    sessionRepository,
		FinancialClaimsInformationRepository: financialClaimsInformationRepository,
		ConfigurationExpirationMinutes:       configurationExpirationMinutes,
	}
}

func (uc *CitizenFinancialClaimUseCase) ConfigureClaimsRequest(sessionToken string, encodedDocumentJWEString string) (string, error) {
	session, err := uc.SessionRepository.GetSession(sessionToken)
	if err != nil {
		event := events.CFCP_3
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("get session failed error, for session (%v): %v", sessionToken, err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		event := events.CFCP_4
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))
		return "", ErrSessionExpired
	}

	if session.Scope != "nl.vorijk.oauth_scope.blauwe_knop" {
		event := events.CFCP_47
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("certificate with scope %s is unsupported", session.Scope)
	}

	sessionAesKey, err := session.AesKey.ToBytes()
	if err != nil {
		event := events.CFCP_49
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to parse session aes key to bytes: %v", err)
	}

	signedDocumentJWS, err := jwe.ToPlaintext(sessionAesKey, encodedDocumentJWEString)
	if err != nil {
		event := events.CFCP_52
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", err
	}

	ecAppPublicKey, err := ec.ParsePublicKeyFromPem([]byte(session.AppPublicKey))
	if err != nil {
		event := events.CFCP_21
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode public key from pem: %v", err)
	}

	verified, err := jws.Verify(string(signedDocumentJWS), ecAppPublicKey)
	if err != nil {
		event := events.CFCP_56
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("failed to verify JWS document signature: %v", err)
	}
	if !verified {
		event := events.CFCP_23
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", errors.New("invalid document signature")
	}

	expiresAt := time.Now().Add(time.Minute * time.Duration(uc.ConfigurationExpirationMinutes))
	configuration := serviceModel.Configuration{
		Token:        uuid.NewString(),
		AppPublicKey: session.AppPublicKey,
		Bsn:          session.Bsn,
		CreatedAt:    serviceModel.JSONTime(time.Now()),
		ExpiresAt:    serviceModel.JSONTime(expiresAt),
	}

	createdConfiguration, err := uc.FinancialClaimRequestRepository.Create(configuration)
	if err != nil {
		event := events.CFCP_24
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to create configuration: %v", err)
	}

	event := events.CFCP_2
	uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("configurationToken", createdConfiguration.Token), zap.Reflect("event", event))

	configurationTokenPayloadAsJson, err := model.DefaultConfigurationTokenPayload(createdConfiguration.Token).AsJson()
	if err != nil {
		event := events.CFCP_53
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", err
	}
	configurationTokenJwsDocument, err := jws.NewDefaultDocument([]byte(configurationTokenPayloadAsJson))
	if err != nil {
		event := events.CFCP_54
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", err
	}
	configurationTokenJWEDocument, err := jwe.NewDefaultDocument(
		sessionAesKey,
		[]byte(configurationTokenJwsDocument))
	if err != nil {
		event := events.CFCP_55
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", err
	}
	return configurationTokenJWEDocument, nil
}

func (uc *CitizenFinancialClaimUseCase) RequestFinancialClaimsInformation(sessionToken string, configurationToken string) (base64.Base64String, error) {
	session, err := uc.SessionRepository.GetSession(sessionToken)
	if err != nil {
		event := events.CFCP_3
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		event := events.CFCP_4
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("sessionToken", sessionToken), zap.Reflect("event", event))
		return "", ErrSessionExpired
	}

	if session.Scope != "nl.vorijk.oauth_scope.blauwe_knop" {
		event := events.CFCP_47
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("certificate with scope %s is unsupported", session.Scope)
	}

	configuration, err := uc.FinancialClaimRequestRepository.Get(configurationToken)
	if err != nil {
		event := events.CFCP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to retrieve configuration: %v", err)
	}

	if time.Now().After((time.Time)(configuration.ExpiresAt)) {
		event := events.CFCP_28
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", ErrConfigurationExpired
	}

	if session.Bsn != configuration.Bsn {
		event := events.CFCP_50
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", ErrInvalidRequest
	}

	financialClaimsInformationDocument, err := uc.FinancialClaimsInformationRepository.Get(sourceSystemModel.UserIdentity{Bsn: sourceSystemModel.Bsn(configuration.Bsn)})
	if err != nil {
		event := events.CFCP_30
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to fetch financialClaimsInformationDocument: %v", err)
	}

	sessionAesKey, err := session.AesKey.ToBytes()
	if err != nil {
		event := events.CFCP_49
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to parse session aes key to bytes: %v", err)
	}

	nonce, encryptedFinancialClaimsInformationDocumentCiphertext, authenticationTag, err := aes.Encrypt(sessionAesKey, *financialClaimsInformationDocument)
	if err != nil {
		event := events.CFCP_34
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to encrypt envelope: %v", err)
	}

	// append for now, this will be updated to a JWE Document in ticket 814
	encryptedFinancialClaimsInformationDocument := append(nonce, encryptedFinancialClaimsInformationDocumentCiphertext...)
	encryptedFinancialClaimsInformationDocument = append(encryptedFinancialClaimsInformationDocument, authenticationTag...)
	encodedFinancialClaimsInformationDocument := base64.ParseFromBytes(encryptedFinancialClaimsInformationDocument)

	return encodedFinancialClaimsInformationDocument, nil
}

func (uc *CitizenFinancialClaimUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.FinancialClaimRequestRepository,
	}
}
