// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

type FinancialClaimsInformationRepository interface {
	Get(id model.UserIdentity) (*[]byte, error)
	healthcheck.Checker
}
