// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	sourceSystemRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/repositories"
)

type FinancialClaimsInformationClient struct {
	baseURL string
	log     *zap.Logger
}

var ErrFinancialClaimsInformationNotFound = errors.New("financial claims information does not exist")

func NewFinancialClaimsInformationClient(baseURL string, log *zap.Logger) *FinancialClaimsInformationClient {
	return &FinancialClaimsInformationClient{
		baseURL: baseURL,
		log:     log,
	}
}

func (s *FinancialClaimsInformationClient) Get(id model.UserIdentity) (*[]byte, error) {
	url := fmt.Sprintf("%s/financial_claims_information", s.baseURL)

	requestBodyAsJson, err := json.Marshal(id)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	// Note: post instead of get as bsn is being send with this request.
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post financial claims information request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get financial claims information: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrFinancialClaimsInformationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving financial claims information: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	return &body, nil
}

func (s *FinancialClaimsInformationClient) GetHealthCheck() healthcheck.Result {
	client := sourceSystemRepositories.NewFinancialClaimsInformationClient(s.baseURL, s.log)
	return client.GetHealthCheck()
}
