// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package versions

var CitizenFinancialClaimsProcessComponentVersion = "reference-0.10.2"
var CitizenFinancialClaimsProcessApiVersion = "v3"
var RequestDocumentVersion = "0.0.1"
var FinancialClaimsInformationDocumentVersion = "v3.0.1"
