// Copyright © Centraal Justitieel Incassobureau (CJIB) 2025
// Licensed under the EUPL

package model

import (
	"encoding/json"
	"fmt"

	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

/**
JwtClaims, the base of all JWT payloads.
*/

type JwtClaims struct {
	Iss string `json:"iss,omitempty"`
	Sub string `json:"sub"`
	Aud string `json:"aud,omitempty"`
	Exp int64  `json:"exp,omitempty"`
	Nbf int64  `json:"nbf,omitempty"`
	Iat int64  `json:"iat,omitempty"`
	Jti string `json:"jti,omitempty"`
}

type VorijkPayloadType string

const (
	Document           VorijkPayloadType = "document"
	ConfigurationToken VorijkPayloadType = "configuration_token"
)

// DocumentPayload - The Document specific payload
type DocumentPayload struct {
	*JwtClaims
	Type    string `json:"type"`
	Version string `json:"version,omitempty"`
	Body    string `json:"body,omitempty"`
}

func DefaultDocumentPayload(documentType string) *DocumentPayload {
	return &DocumentPayload{
		JwtClaims: &JwtClaims{
			Sub: string(Document),
		},
		Type: documentType,
	}
}

func DocumentPayloadFromEncodedJson(encodedDocumentPayloadAsJson base64.Base64String) (*DocumentPayload, error) {
	documentPayloadAsJson, err := encodedDocumentPayloadAsJson.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding Document Payload. Error: %v", err)
	}
	return DocumentPayloadFromJson(documentPayloadAsJson)
}

func DocumentPayloadFromJson(documentPayloadAsJson []byte) (*DocumentPayload, error) {
	var documentPayload DocumentPayload
	err := json.Unmarshal(documentPayloadAsJson, &documentPayload)
	if err != nil {
		return nil, fmt.Errorf("error occurred while unmarshalling Document Payload. Error: %v", err)
	}
	return &documentPayload, nil
}

func (p *DocumentPayload) AsEncodedJson() (base64.Base64String, error) {
	documentPayloadAsString, err := p.AsJson()
	if err != nil {
		return "", err
	}
	return base64.ParseFromBytes([]byte(documentPayloadAsString)), nil
}

func (p *DocumentPayload) AsJson() (string, error) {
	documentPayloadAsJson, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("error occurred while marshalling Document Payload. Error: %v", err)
	}
	return string(documentPayloadAsJson), nil
}

// ConfigurationTokenPayload - The ConfigurationToken specific payload
type ConfigurationTokenPayload struct {
	*JwtClaims
	ConfigurationToken string `json:"configuration_token,omitempty"`
}

func DefaultConfigurationTokenPayload(configurationToken string) *ConfigurationTokenPayload {
	return &ConfigurationTokenPayload{
		JwtClaims: &JwtClaims{
			Sub: string(ConfigurationToken),
			Iss: "https://citizen-financial-claims-process.example.com/",
		},
		ConfigurationToken: configurationToken,
	}
}

func ConfigurationTokenPayloadFromEncodedJson(encodedConfigurationTokenPayloadAsJson base64.Base64String) (*ConfigurationTokenPayload, error) {
	configurationTokenPayloadAsJson, err := encodedConfigurationTokenPayloadAsJson.ToBytes()
	if err != nil {
		return nil, fmt.Errorf("error occurred while Base64 decoding ConfigurationToken Payload. Error: %v", err)
	}
	return ConfigurationTokenPayloadFromJson(configurationTokenPayloadAsJson)
}

func ConfigurationTokenPayloadFromJson(configurationTokenPayloadAsJson []byte) (*ConfigurationTokenPayload, error) {
	var configurationTokenPayload ConfigurationTokenPayload
	err := json.Unmarshal(configurationTokenPayloadAsJson, &configurationTokenPayload)
	if err != nil {
		return nil, fmt.Errorf("error occurred while unmarshalling ConfigurationToken Payload. Error: %v", err)
	}
	return &configurationTokenPayload, nil
}

func (p *ConfigurationTokenPayload) AsEncodedJson() (base64.Base64String, error) {
	configurationTokenPayloadAsString, err := p.AsJson()
	if err != nil {
		return "", err
	}
	return base64.ParseFromBytes([]byte(configurationTokenPayloadAsString)), nil
}

func (p *ConfigurationTokenPayload) AsJson() (string, error) {
	configurationTokenPayloadAsJson, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("error occurred while marshalling ConfigurationToken Payload. Error: %v", err)
	}
	return string(configurationTokenPayloadAsJson), nil
}
