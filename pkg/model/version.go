// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Version struct {
	CitizenFinancialClaimsProcessComponentVersion string `json:"citizenFinancialClaimsProcessComponentVersion"`
	CitizenFinancialClaimsProcessApiVersion       string `json:"citizenFinancialClaimsProcessApiVersion"`
	RequestDocumentVersion                        string `json:"requestDocumentVersion"`
	FinancialClaimsInformationDocumentVersion     string `json:"financialClaimsInformationDocumentVersion"`
}
