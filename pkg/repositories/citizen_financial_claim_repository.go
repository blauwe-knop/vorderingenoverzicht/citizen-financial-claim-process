// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

type CitizenFinancialClaimRepository interface {
	ConfigureClaimsRequest(sessionToken string, encryptedEnvelope base64.Base64String) (base64.Base64String, error)
	RequestFinancialClaimsInformation(sessionToken string, configurationToken string) (base64.Base64String, error)
	GetHealth() error
	healthcheck.Checker
}
