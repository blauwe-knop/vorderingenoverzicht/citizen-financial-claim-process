// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"
)

type CitizenFinancialClaimClient struct {
	baseURL string
}

func NewCitizenFinancialClaimClient(baseURL string) *CitizenFinancialClaimClient {
	return &CitizenFinancialClaimClient{
		baseURL: baseURL,
	}
}

func (c *CitizenFinancialClaimClient) ConfigureClaimsRequest(sessionToken string, encryptedEnvelope base64.Base64String) (base64.Base64String, error) {
	url := fmt.Sprintf("%s/configure_claims_request", c.baseURL)

	requestBodyAsJson, err := json.Marshal(encryptedEnvelope)
	if err != nil {
		return "", fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return "", fmt.Errorf("failed to post ConfigureClaimsRequest request: %v", err)
	}

	request.Header.Set("Authorization", sessionToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("failed to post ConfigureClaimsRequest: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status while retrieving ConfigureClaimsRequest: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %v", err)
	}

	var encryptedConfigurationToken base64.Base64String
	err = json.Unmarshal(body, &encryptedConfigurationToken)
	if err != nil {
		return "", fmt.Errorf("failed to decode json, Configuration with token: %s: %v", sessionToken, err)
	}

	return encryptedConfigurationToken, nil
}
func (c *CitizenFinancialClaimClient) RequestFinancialClaimsInformation(sessionToken string, configurationToken string) (base64.Base64String, error) {
	url := fmt.Sprintf("%s/request_financial_claims_information?configurationToken=%s", c.baseURL, configurationToken)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", fmt.Errorf("failed to get RequestFinancialClaimsInformation request: %v", err)
	}

	request.Header.Set("Authorization", sessionToken)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("failed to get RequestFinancialClaimsInformation: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status while retrieving RequestFinancialClaimsInformation: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %v", err)
	}

	var financialClaimsInformation base64.Base64String
	err = json.Unmarshal(body, &financialClaimsInformation)
	if err != nil {
		return "", fmt.Errorf("failed to decode json, FinancialClaimsInformation with token: %s: %v", sessionToken, err)
	}

	return financialClaimsInformation, nil
}

func (c *CitizenFinancialClaimClient) GetHealth() error {
	url := fmt.Sprintf("%s/health", c.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch citizen financial claim process health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch citizen financial claim process health with status: %d", resp.StatusCode)
	}

	return nil
}

func (c *CitizenFinancialClaimClient) GetHealthCheck() healthcheck.Result {
	name := "citizen-financial-claim-process"
	url := fmt.Sprintf("%s/health/check", c.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
