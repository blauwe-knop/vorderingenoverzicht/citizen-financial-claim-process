package events

var (
	CFCP_1 = NewEvent(
		"cfcp_1",
		"received request for financial claims configuration",
		Low,
	)
	CFCP_2 = NewEvent(
		"cfcp_2",
		"created configuration token",
		Low,
	)
	CFCP_3 = NewEvent(
		"cfcp_3",
		"failed to get session",
		High,
	)
	CFCP_4 = NewEvent(
		"cfcp_4",
		"session expired",
		Medium,
	)
	CFCP_5 = NewEvent(
		"cfcp_5",
		"failed to get private key",
		High,
	)
	CFCP_6 = NewEvent(
		"cfcp_6",
		"failed to decode private key pem",
		High,
	)
	// Deprecated: CFCP_7 is deprecated
	CFCP_7 = NewEvent(
		"cfcp_7",
		"failed to decode ciphertext from envelope",
		High,
	)
	// Deprecated: CFCP_8 is deprecated
	CFCP_8 = NewEvent(
		"cfcp_8",
		"failed to decrypt envelope",
		High,
	)
	// Deprecated: CFCP_9 is deprecated
	CFCP_9 = NewEvent(
		"cfcp_9",
		"failed to unmarshal",
		High,
	)
	CFCP_10 = NewEvent(
		"cfcp_10",
		"unknown certificate",
		High,
	)
	CFCP_11 = NewEvent(
		"cfcp_11",
		"failed to parse token unverified",
		High,
	)
	CFCP_12 = NewEvent(
		"cfcp_12",
		"failed to fetch app manager",
		High,
	)
	CFCP_13 = NewEvent(
		"cfcp_13",
		"failed to parse app manager public key",
		High,
	)
	CFCP_14 = NewEvent(
		"cfcp_14",
		"unexpected JWT signing method",
		High,
	)
	CFCP_15 = NewEvent(
		"cfcp_15",
		"certificate expired",
		High,
	)
	CFCP_16 = NewEvent(
		"cfcp_16",
		"failed to verify and parse token",
		High,
	)
	CFCP_17 = NewEvent(
		"cfcp_17",
		"invalid request",
		High,
	)
	CFCP_18 = NewEvent(
		"cfcp_18",
		"AppManagerPublicKey invalid",
		High,
	)
	CFCP_19 = NewEvent(
		"cfcp_19",
		"envelope.Certificate.AppPublicKey differs from the session.AppPublicKey",
		High,
	)
	// Deprecated: CFCP_20 is deprecated
	CFCP_20 = NewEvent(
		"cfcp_20",
		"failed to marshal document",
		High,
	)
	CFCP_21 = NewEvent(
		"cfcp_21",
		"failed to decode public key from pem",
		High,
	)
	CFCP_22 = NewEvent(
		"cfcp_22",
		"failed to verify document signature",
		High,
	)
	CFCP_23 = NewEvent(
		"cfcp_23",
		"invalid document signature",
		High,
	)
	CFCP_24 = NewEvent(
		"cfcp_24",
		"failed to create configuration",
		High,
	)
	CFCP_25 = NewEvent(
		"cfcp_25",
		"received request for financial claims information",
		Low,
	)
	CFCP_26 = NewEvent(
		"cfcp_26",
		"sent response with financial claims information",
		Low,
	)
	CFCP_27 = NewEvent(
		"cfcp_27",
		"failed to retrieve configuration",
		High,
	)
	CFCP_28 = NewEvent(
		"cfcp_28",
		"configuration expired",
		High,
	)
	// Deprecated: CFCP_29 is deprecated
	CFCP_29 = NewEvent(
		"cfcp_29",
		"configuration.AppPublicKey differs from the session.AppPublicKey",
		High,
	)
	CFCP_30 = NewEvent(
		"cfcp_30",
		"failed to fetch financialClaimsInformationDocument",
		High,
	)
	// Deprecated: CFCP_31 is deprecated
	CFCP_31 = NewEvent(
		"cfcp_31",
		"overriding financial claims information document",
		Low,
	)
	// Deprecated: CFCP_32 is deprecated
	CFCP_32 = NewEvent(
		"cfcp_32",
		"appending financial claims information document",
		Low,
	)
	// Deprecated: CFCP_33 is deprecated
	CFCP_33 = NewEvent(
		"cfcp_33",
		"failed to marshal",
		High,
	)
	CFCP_34 = NewEvent(
		"cfcp_34",
		"failed to encrypt envelope",
		High,
	)
	CFCP_35 = NewEvent(
		"cfcp_35",
		"failed to list key pairs",
		High,
	)
	CFCP_36 = NewEvent(
		"cfcp_36",
		"key pair does not exist",
		High,
	)
	CFCP_37 = NewEvent(
		"cfcp_37",
		"failed to encode response payload",
		High,
	)
	CFCP_38 = NewEvent(
		"cfcp_38",
		"failed to configure claims request",
		High,
	)
	CFCP_39 = NewEvent(
		"cfcp_39",
		"failed to decode request payload",
		High,
	)
	CFCP_40 = NewEvent(
		"cfcp_40",
		"failed to write file bytes",
		High,
	)
	CFCP_41 = NewEvent(
		"cfcp_41",
		"failed to configure claims request",
		High,
	)
	CFCP_42 = NewEvent(
		"cfcp_42",
		"started listening",
		Low,
	)
	CFCP_43 = NewEvent(
		"cfcp_43",
		"server closed",
		High,
	)
	CFCP_44 = NewEvent(
		"cfcp_44",
		"sent response with financial claims configuration",
		Low,
	)
	CFCP_45 = NewEvent(
		"cfcp_45",
		"failed to parse configurationExpirationMinutes to int",
		VeryHigh,
	)
	// Deprecated: CFCP_46 is deprecated
	CFCP_46 = NewEvent(
		"cfcp_46",
		"failed to encrypt session token",
		High,
	)
	CFCP_47 = NewEvent(
		"cfcp_47",
		"session with scope other than 'nl.vorijk.oauth_scope.blauwe_knop' is currently not supported",
		High,
	)
	// Deprecated: CFCP_48 is deprecated
	CFCP_48 = NewEvent(
		"cfcp_48",
		"failed to base64 encode document signature",
		High,
	)
	CFCP_49 = NewEvent(
		"cfcp_49",
		"failed to parse session aes key to bytes",
		High,
	)
	CFCP_50 = NewEvent(
		"cfcp_50",
		"session bsn not equal to configuration bsn",
		High,
	)
	// Deprecated: CFCP_51 is deprecated
	CFCP_51 = NewEvent(
		"cfcp_51",
		"failed to decrypt configuration token",
		High,
	)
	CFCP_52 = NewEvent(
		"cfcp_52",
		"failed to decrypt ciphertext from JWE Document",
		High,
	)
	CFCP_53 = NewEvent(
		"cfcp_53",
		"failed to marshal configuration token payload into JSON",
		High,
	)
	CFCP_54 = NewEvent(
		"cfcp_54",
		"failed to create new configuration token JWS Document from configuration token payload",
		High,
	)
	CFCP_55 = NewEvent(
		"cfcp_55",
		"failed to create new configuration token JWE Document from configuration token JWS Document",
		High,
	)
	CFCP_56 = NewEvent(
		"cfcp_56",
		"failed to verify JWS document signature",
		High,
	)
)
